import React, {useEffect} from 'react';
import { View, Text, Image, StyleSheet, StatusBar} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('Home')
        }, 3000)
    })
    return (
        <View style={style.back}>
            <StatusBar backgroundColor={'black'} barStyle='light-content' />
            <Image
                source={require('../../assets/images/backg.jpg')}
                style={style.backg}
            /> 
            {/* <Text style={style.item}>Splash Screen</Text> */}
        </View>
    );
};

export default Splash
const style = StyleSheet.create ({
    item:{
        alignSelf:'center',
        position:'absolute'
    },
    back:{
        flex:1,
        justifyContent:'center'
    },
    backg:{
        width:'100%',
        height:'100%',
    }
})