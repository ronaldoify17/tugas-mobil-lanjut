import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

const Model = ({navigation}) => {
  return (
    <View>
      <Text style={styles.judul}>Daftar Orang Gila</Text>
      <Button
        title='Pindah'
        onPress={()=>navigation.navigate('Splash')}
      />
    </View>
  )
}

export default Model

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginBottom:20,
  },
  tombol:{
    width:40,
    height:20,
    backgroundColor:'black'
  },
})